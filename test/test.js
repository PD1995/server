var assert = require('assert');
describe('Hello World Module', function() {
    it('should return -1 when "Hello" is missing', function() {
        assert.equal(-1, "Hallo World".indexOf("Hello"));
    });
    it('should return 0 when sentence start with "Hello"', function() {
        assert.equal(0, "Hello World, how are you?".indexOf("Hello"));
    });
});